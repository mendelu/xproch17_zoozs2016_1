#ifndef RYTIR_H
#define RYTIR_H

#include <vector>
#include "Brneni.h"
#include "Lektvar.h"

class Rytir {

private:
	int m_zivot;
	int m_sila;
	Brneni* m_neseneBrneni;
	std::vector<Lektvar*> m_inventar;

public:
	Rytir(int zivot, int sila, int bonusObranyBrneni);
	~Rytir();
	void seberLektvar(Lektvar* nalezenyLektvar);
	void vypijLektvar();
	void printInfo();
};

#endif // RYTIR_H
