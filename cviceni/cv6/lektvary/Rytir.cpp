#include "Rytir.h"

Rytir::Rytir(int zivot, int sila, int bonusObranyBrneni) {
	m_zivot = zivot;
	m_sila = sila;
	m_neseneBrneni = new Brneni(bonusObranyBrneni);
}

Rytir::~Rytir() {
	delete m_neseneBrneni;
}

void Rytir::seberLektvar(Lektvar* nalezenyLektvar) {
	/// jestize mam u sebe mene nez 10 lektvaru
	/// tak ho seberu
	if (m_inventar.size() < 10){
        m_inventar.push_back(nalezenyLektvar);
	} else {
        /// pokud ne, tak vypisu, ze jsem plny
        std::cout << "Nemam misto" << std::endl;
	}
}

void Rytir::vypijLektvar() {
    /// pristoupim k poslednimu lektvaru a zjistim jeho hodnoty
    /// ty prictu ke svym hodnotam
    int indexPoslednihoLekt = m_inventar.size()-1;
    Lektvar* posledniLekvar = m_inventar.at(indexPoslednihoLekt);
    m_sila += posledniLekvar->getBonusSily();
    m_zivot += posledniLekvar->getBonusZivota();

    /// odstranim posledni lahvicku
	m_inventar.pop_back();
}

void Rytir::printInfo() {
	std::cout << "Zivot: " << m_zivot << std::endl;
	std::cout << "Sila: " << m_sila << std::endl;
}
