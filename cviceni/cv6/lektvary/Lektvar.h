#ifndef LEKTVAR_H
#define LEKTVAR_H

#include <iostream>

class Lektvar {

private:
	int m_bonusZivota;
	int m_bonusSily;
	std::string m_nazev;

public:
	Lektvar(int bonusZivota, int bonusSily, std::string nazev);
	int getBonusZivota();
	int getBonusSily();
	std::string getNazev();
};
#endif
