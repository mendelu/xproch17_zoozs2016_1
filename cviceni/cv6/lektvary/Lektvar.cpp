#include "Lektvar.h"

Lektvar::Lektvar(int bonusZivota, int bonusSily, std::string nazev) {
	m_bonusSily = bonusSily;
	m_bonusZivota = bonusZivota;
	m_nazev = nazev;
}

int Lektvar::getBonusZivota() {
	return m_bonusZivota;
}

int Lektvar::getBonusSily() {
	return m_bonusSily;
}

std::string Lektvar::getNazev() {
	return m_nazev;
}
