#include <iostream>
#include "Rytir.h"
#include "Lektvar.h"
using namespace std;

int main()
{
    Rytir* artus = new Rytir(100, 10, 25);
    Lektvar* napojSily = new Lektvar(0, 12, "Napoj Sily");
    artus->seberLektvar(napojSily);

    Lektvar* napojZdravi = new Lektvar(10, 0, "Napoj zdravi");
    artus->seberLektvar(napojZdravi);

    Lektvar* redBull = new Lektvar(-10, 10, "Red Bull");
    artus->seberLektvar(redBull);

    artus->vypijLektvar();
    artus->vypijLektvar();

    artus->printInfo();

    delete redBull;
    delete napojSily;
    delete artus;
    return 0;
}
