#include <iostream>
using namespace std;

class Zviratko{
private:
    float m_kalorie;
    float m_vaha;
public:
    Zviratko(float kalorie, float vaha){
        m_kalorie = kalorie;
        m_vaha = vaha;
    }

    float getKalorie(){
        return m_kalorie;
    }

    float getVaha(){
        return m_vaha;
    }
};

class Princ{
private:
    float m_kalorie;
    float m_vaha;
    string m_jmeno;
public:
    Princ(float kalorie, float vaha, string jmeno){
        m_kalorie = kalorie;
        m_vaha = vaha;
    }

    float getKalorie(){
        return m_kalorie;
    }

    float getVaha(){
        return m_vaha;
    }

    string getJmeno(){
        return m_jmeno;
    }
};

class Drak{
private:
    float m_sezraneKalorie;
    float m_sezranaVaha;
public:
    Drak(){
        m_sezranaVaha = 0;
        m_sezraneKalorie = 0;
    }

    Drak(float sezraneKalorie, float sezranaVaha){
        m_sezranaVaha = sezranaVaha;
        m_sezraneKalorie = sezraneKalorie;
    }

    void sezer(float sezraneKalorie, float sezranaVaha){
        m_sezraneKalorie += sezraneKalorie;
        m_sezranaVaha += sezranaVaha;
    }

    void sezer(Princ* nejakyPrinc){
        m_sezraneKalorie += nejakyPrinc->getKalorie();
        m_sezranaVaha += nejakyPrinc->getVaha();
    }

    void printInfo(){
        cout << "Sezrane kalorie: " << m_sezraneKalorie << endl;
        cout << "Sezrana vaha: " << m_sezranaVaha << endl;
    }
};

int main()
{
    Zviratko* veverka = new Zviratko(10, 1);
    Princ* artus = new Princ(10000, 89, "Artus");
    Drak* karel = new Drak();

    //float pocetKaloriiVeverky = veverka->getKalorie();
    karel->sezer(veverka->getKalorie(),veverka->getVaha());
    karel->printInfo();
    karel->sezer(artus);
    //karel->sezer(artus->getVaha(), artus->getKalorie());

    karel->printInfo();

    delete karel;
    delete artus;
    delete veverka;
    return 0;
}
