#include <iostream>
#include "Clovek.h"
#include "Elf.h"

int main()
{
    Bytost* nepritel1 = nullptr;
    Bytost* nepritel2 = nullptr;

    std::string rozhodnuti;
    std::cout << "Vyber nepritele c.1 [e]lf, [c]lovek: ";
    std::cin >> rozhodnuti;

    if (rozhodnuti == "e"){
        nepritel1 = new Elf("Elf Elfovic", 10, 10, 10);
    } else {
        nepritel1 = new Clovek("Karel Kos", 10, 10);
    }

    std::cout << "Vyber nepritele c.2 [e]lf, [c]lovek: ";
    std::cin >> rozhodnuti;

    if (rozhodnuti == "e"){
        nepritel2 = new Elf("Elf Eleven", 10, 10, 10);
    } else {
        nepritel2 = new Clovek("Pepa Kos", 9, 9);
    }

    nepritel1->printInfo();
    nepritel2->printInfo();
    nepritel1->zautoc(nepritel2);
    nepritel2->zautocMagii(nepritel1);
    nepritel1->zvysUroven();
    nepritel1->printInfo();
    nepritel2->printInfo();

    delete nepritel1;
    delete nepritel2;
    return 0;
}
