#include "Clovek.h"

Clovek::Clovek(std::string jmeno, int sila, int obratnost):
    Bytost(jmeno, sila, obratnost, 0) {

}

void Clovek::zvysUroven() {
	m_sila++;
	m_obratnost += 2;
	m_magie++;
}

void Clovek::printInfo() {
	std::cout << "Jsem clovek " << std::endl;
	Bytost::printInfo();
	std::cout << std::endl;
}
