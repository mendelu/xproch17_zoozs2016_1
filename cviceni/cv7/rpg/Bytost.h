#ifndef BYTOST_H
#define BYTOST_H

#include <iostream>

class Bytost {

protected:
	std::string m_jmeno;
	int m_zivot;
	int m_sila;
	int m_obratnost;
	int m_magie;

public:
	Bytost(std::string jmeno, int sila, int obratnost, int magie);
	void zautoc(Bytost* nepritel);
	void zautocMagii(Bytost* nepritel);
	int getMagie();
	int getUtok();
	void udelZraneni(int zraneni);
	virtual void printInfo();
	virtual void zvysUroven();
};

#endif // BYTOST_H
