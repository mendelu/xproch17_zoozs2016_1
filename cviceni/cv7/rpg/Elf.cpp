#include "Elf.h"

Elf::Elf(std::string jmeno, int sila, int obratnost, int magie):
    Bytost(jmeno, sila, obratnost, magie) {

}

void Elf::zvysUroven() {
	/// s rostouci urovni klesa jeho sila az na pet
	if (m_sila > 5){
        m_sila--;
	}
	m_obratnost += 2;
	m_magie += 2;
}

void Elf::printInfo() {
	std::cout << "Jsem elf " << std::endl;
	Bytost::printInfo();
	std::cout << std::endl;
}
