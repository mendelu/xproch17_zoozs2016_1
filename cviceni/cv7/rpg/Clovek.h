#ifndef CLOVEK_H
#define CLOVEK_H

#include <iostream>
#include "Bytost.h"

class Clovek : public Bytost {

public:
	Clovek(std::string jmeno, int sila, int obratnost);
	void zvysUroven();
	void printInfo();
};

#endif // CLOVEK_H
