#include "Bytost.h"

Bytost::Bytost(std::string jmeno, int sila, int obratnost, int magie) {
    m_jmeno = jmeno;
    m_sila = sila;
    m_obratnost = obratnost;
    m_magie = magie;
    m_zivot = 100;
}

void Bytost::zautoc(Bytost* nepritel) {

	/// jestlize jsem silnejsi, udelim zraneni
	if (getUtok() > nepritel->getUtok()){
        nepritel->udelZraneni( getUtok() - nepritel->getUtok() );
	} else {
	/// v opacnem pripade dostanu zraneni ja
        udelZraneni( nepritel->getUtok() - getUtok() );
	}
}

void Bytost::zautocMagii(Bytost* nepritel) {
	/// jestlize jsem silnejsi, udelim zraneni
	if (m_magie > nepritel->getMagie()){
        nepritel->udelZraneni( m_magie - nepritel->getMagie() );
	} else {
	/// v opacnem pripade dostanu zraneni ja
        udelZraneni( nepritel->getMagie() - m_magie );
	}
}

int Bytost::getMagie() {
	return m_magie;
}

int Bytost::getUtok() {
	return m_sila*m_obratnost;
}

void Bytost::udelZraneni(int zraneni) {
	/// jestlize by zraneni bylo vetsi nez stavajici zivot
	/// nastavime zivot na nula a vypiseme informaci
	if (m_zivot > zraneni){
        m_zivot -= zraneni;
	} else {
        m_zivot = 0;
        std::cout << "Hrdina " << m_jmeno << " zemrel!" << std::endl;
	}
}

void Bytost::printInfo() {
	std::cout << "Informace o postave " << m_jmeno << std::endl;
	std::cout << "Zivoty:" << m_zivot << std::endl;
	std::cout << "Sila:" << m_sila << std::endl;
	std::cout << "Obratnost:" << m_obratnost << std::endl;
	std::cout << "Magie:" << m_magie << std::endl;
}

void Bytost::zvysUroven() {
	m_sila++;
	m_obratnost++;
	m_magie++;
}
