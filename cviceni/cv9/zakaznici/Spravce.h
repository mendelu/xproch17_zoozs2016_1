#ifndef SPRAVCE_H
#define SPRAVCE_H

#include <iostream>
#include <vector>
#include "Zakaznik.h"
class Spravce {

private:
	static std::vector<Zakaznik*> s_zakaznici;
	static int s_pocetZakazniku;

public:
	static Zakaznik* getZakaznikById(int id);
	static std::vector<Zakaznik*> getZakaznikByName(std::string jmeno);
	static Zakaznik* createZakaznik(std::string jmeno);
	static void printAllCustomers();

private:
	Spravce();
};
#endif // SPRAVCE_H
