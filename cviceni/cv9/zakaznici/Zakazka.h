#ifndef ZAKAZKA_H
#define ZAKAZKA_H

#include <iostream>
#include <cmath>
#include "Zakaznik.h"
#include "Spravce.h"

class Zakazka {
private:
	std::string m_popis;
	int m_naklady;
	Zakaznik* m_zakaznik;
public:
	Zakazka(std::string popis, int naklady, Zakaznik* zakaznik);
	Zakazka(std::string popis, int naklady, int idZakaznika);
	int getFakturovanaCena();
	void printInfo();
};
#endif // ZAKAZKA_H
