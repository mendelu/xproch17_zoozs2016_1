#include "Zakazka.h"

Zakazka::Zakazka(std::string popis, int naklady, Zakaznik* zakaznik) {
	m_popis = popis;
	m_naklady = naklady;
	m_zakaznik = zakaznik;
}

Zakazka::Zakazka(std::string popis, int naklady, int idZakaznika) {
	m_popis = popis;
	m_naklady = naklady;

	Zakaznik* novyZakaznik = Spravce::getZakaznikById(idZakaznika);
	if (novyZakaznik == nullptr){
        std::cout << "Zakaznik s id " << idZakaznika << " neexistuje!" << std::endl;
	}
	m_zakaznik = novyZakaznik;
}

int Zakazka::getFakturovanaCena() {
	return round(m_naklady*1.5);
}

void Zakazka::printInfo() {
	std::cout << "Cena: " << getFakturovanaCena() << std::endl;
    std::cout << "Objednal: " << m_zakaznik->getJmeno() << std::endl;
    std::cout << "Popis: " << m_popis << std::endl;
}
