#ifndef ZAKAZNIK_H
#define ZAKAZNIK_H

#include <iostream>

class Zakaznik {
private:
	int m_id;
	std::string m_jmeno;
public:
	Zakaznik(int id, std::string jmeno);
	std::string getJmeno();
	int getId();
};
#endif // ZAKAZNIK_H
