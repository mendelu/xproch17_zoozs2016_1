#include "Zakaznik.h"

Zakaznik::Zakaznik(int id, std::string jmeno) {
	m_id = id;
	m_jmeno = jmeno;
}

std::string Zakaznik::getJmeno() {
	return m_jmeno;
}

int Zakaznik::getId() {
	return m_id;
}
