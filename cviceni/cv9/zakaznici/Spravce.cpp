#include "Spravce.h"

std::vector<Zakaznik*> Spravce::s_zakaznici = {};
int Spravce::s_pocetZakazniku = 0;

Zakaznik* Spravce::getZakaznikById(int id) {
	for (Zakaznik* actZakaznik:s_zakaznici){
        if (actZakaznik->getId() == id){
            return actZakaznik;
        }
	}
	return nullptr;
}

std::vector<Zakaznik*> Spravce::getZakaznikByName(std::string jmeno) {
	std::vector<Zakaznik*> zakazniciSeJmenem;

	for (Zakaznik* actZakaznik:s_zakaznici){
        if (actZakaznik->getJmeno() == jmeno){
            zakazniciSeJmenem.push_back(actZakaznik);
        }
	}

	return zakazniciSeJmenem;
}

Zakaznik* Spravce::createZakaznik(std::string jmeno) {
	Zakaznik* novyZakaznik
        = new Zakaznik(s_pocetZakazniku, jmeno);
    s_pocetZakazniku++;
    s_zakaznici.push_back(novyZakaznik);
    return novyZakaznik;
}

void Spravce::printAllCustomers() {
	for (Zakaznik* actZakaznik:s_zakaznici){
        std::cout << "Jmeno: " << actZakaznik->getJmeno() << std::endl;
	}
}

Spravce::Spravce() {
}
