#include <iostream>
#include "Spravce.h"
#include "Zakazka.h"
using namespace std;

int main()
{
    Zakaznik* karel = Spravce::createZakaznik("Karel Kos");
    int karlovoId = karel->getId();

    Zakazka* oprava = new Zakazka("oprava vody", 100, karlovoId);
    oprava->printInfo();
    return 0;
}
