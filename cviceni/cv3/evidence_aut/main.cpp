#include <iostream>
using namespace std;

class SluzebniAuto{
private:
    string m_jmenoPoslednihoRidice;
    float m_aktualniUcetniCena;
    float m_amortizaceZaKm;
    float m_pocetNajetychKm;

public:

    SluzebniAuto(float pocatecniCena){
        m_jmenoPoslednihoRidice = "";
        m_aktualniUcetniCena = pocatecniCena;
        m_amortizaceZaKm = 20;
        m_pocetNajetychKm = 0;
    }

    SluzebniAuto(float pocatecniCena, float amortizaceZaKm){
        m_jmenoPoslednihoRidice = "";
        m_aktualniUcetniCena = pocatecniCena;
        m_amortizaceZaKm = amortizaceZaKm;
        m_pocetNajetychKm = 0;
    }

    void pridejJizdu(float pocetUjetychKm, string ridic){
        /// ulozilo jmeno posledniho ridice
        m_jmenoPoslednihoRidice = ridic;
        /// zvysi pocet najetych km o novou hodnotu
        m_pocetNajetychKm = m_pocetNajetychKm + pocetUjetychKm;
        //m_pocetNajetychKm += pocetUjetychKm;
        /// snizila cena auta o amortizaci ktera odpovida jizde
        if (vyskaAmortizaceJizdy(pocetUjetychKm) < m_aktualniUcetniCena){
            m_aktualniUcetniCena = 0;
        } else {
            m_aktualniUcetniCena -= vyskaAmortizaceJizdy(pocetUjetychKm);
        }
    }

    string getPosledniRidic(){
        // tohle ...
        return m_jmenoPoslednihoRidice;
    }

    bool jeAmortizovano(){
        /*
        if (m_aktualniUcetniCena == 0){
            return true;
        } else {
            return false;
        }
        */
        return (m_aktualniUcetniCena == 0);
    }

private:
    float vyskaAmortizaceJizdy(float pocetUjetychKm) {
        /*
        float amortizaceJizdy = m_amortizaceZaKm*pocetUjetychKm;
        return amortizaceJizdy;
        */
        return m_amortizaceZaKm*pocetUjetychKm;
    }

};


int main()
{
    SluzebniAuto* skoda = new SluzebniAuto(545000, 26);
    skoda->pridejJizdu(500000, "David");

    if (skoda->jeAmortizovano()){
        cout << "Uz se zaplatila" << endl;
    } else {
        cout << "jeste se nezaplatila" << endl;
    }

    delete skoda;
    return 0;
}
