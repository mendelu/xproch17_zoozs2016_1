#include <iostream>
using namespace std;

class Student{
public:
    string m_jmeno;
    int m_semestrStudia;

	//ahoj

    Student(){
        m_jmeno = "nestanoveno";
        m_semestrStudia = 1;
    }

    Student(string jmeno){
        m_jmeno = jmeno;
        m_semestrStudia = 1;
    }

    Student(string jmeno, int semestr){
        m_jmeno = jmeno;
        m_semestrStudia = semestr;
    }

    void setJmeno(string noveJmeno){
        if (noveJmeno != ""){
            m_jmeno = noveJmeno;
        } else {
            cout << "Jmeno nemuze byt prazdne" << endl;
        }
    }

    void setSemestr(int novySemestr){
        if ((novySemestr > 0) and (novySemestr\ < 10))  {
            m_semestrStudia = novySemestr;
        } else {
            cout << "Semestr musi byt > 0" << endl;
        }
    }

    string getJmeno(){
        if (m_jmeno != ""){
            return m_jmeno;
        } else {
            return "Jmeno nestanoveno";
        }
    }

    int getSemestr(){
        return m_semestrStudia;
    }
};


int main()
{
    Student* david = new Student("David", 3);
    //david->setJmeno("David");
    cout << "Jmeno: " << david->m_jmeno << endl;
    cout << "Jmeno: " << david->getJmeno() << endl;

    cout << "Semestr: " << david->m_semestrStudia << endl;

    delete david;
    return 0;
}
