#ifndef REDITEL
#define REDITEL

#include<iostream>
#include "Zamestnanec.h"
using namespace std;

class Reditel {

private:
	Zamestnanec* m_asistent;
	string m_jmeno;

public:
	Reditel(string jmeno);
	void zmenAsistenta(Zamestnanec* novyAsistent);
	string getJmeno();
	string getJmenoAsistenta();
	void zvysPlatAsistenta(int oKolik);
};

#endif
