#include <iostream>
#include "Reditel.h"
#include "Zamestnanec.h"
using namespace std;

int main()
{
    Reditel* karel = new Reditel("Karel Kos");
    karel->zvysPlatAsistenta(1000);

    cout << "Jmeno asistenta: " << karel->getJmenoAsistenta() << endl;

    Zamestnanec* jan = new Zamestnanec("Jan Novy", 10000);
    karel->zmenAsistenta(jan);
    cout << "Jmeno asistenta: " << karel->getJmenoAsistenta() << endl;
    karel->zvysPlatAsistenta(1000);
    cout << jan->getPlat() << endl;

    delete jan;
    delete karel;
    return 0;
}
