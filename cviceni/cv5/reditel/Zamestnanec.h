#ifndef ZAMESTNANEC
#define ZAMESTNANEC

#include<iostream>
using namespace std;

class Zamestnanec {

private:
	string m_jmeno;
	int m_plat;

public:
	Zamestnanec(string jmeno, int plat);
	string getJmeno();
	int getPlat();
	void zvysPlat(int zvyseni);
};

#endif
