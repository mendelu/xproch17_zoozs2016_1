#include "Reditel.h"

Reditel::Reditel(string jmeno) {
	m_jmeno = jmeno;
	m_asistent = nullptr;
}

void Reditel::zmenAsistenta(Zamestnanec* novyAsistent) {
	m_asistent = novyAsistent;
}

string Reditel::getJmeno() {
	return m_jmeno;
}

string Reditel::getJmenoAsistenta() {
	if (m_asistent != nullptr){
        return m_asistent->getJmeno();
	} else {
        return "";
	}
}

void Reditel::zvysPlatAsistenta(int oKolik){
    if (m_asistent != nullptr){
        m_asistent->zvysPlat(oKolik);
    } else {
        cout << "Neni zadny asistent" << endl;
    }
}
